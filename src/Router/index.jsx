import { BrowserRouter, Routes, Route } from "react-router-dom"
import PageFrame from "../component/PageFrame"
import Home from './page/Home'
import User from './page/User'

export default function Router() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<PageFrame />}>
                    <Route index element={<Home />} />
                    <Route path="user/:userId" element={<User />}/>
                </Route>
            </Routes>
        </BrowserRouter>
    )
}