import { useState, useCallback } from "react"
import Search from '../../component/Search'
import UserList from '../../component/UserList'

export default function Home () {
  const [searchTerm, setSearchTerm] = useState('')

  const onSubmit = useCallback((value) => {
    setSearchTerm(value)
  }, [setSearchTerm])

  return (
    <div className="w-full h-full">

        <Search onSubmit={onSubmit} />

        <UserList searchTerm={searchTerm} />
    </div>
  )
}
