import Router from './Router'
import './App.css';
import 'react-loading-skeleton/dist/skeleton.css'

export default function App() {
  return (
    <Router />
  );
}
