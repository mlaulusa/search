import { useState, useEffect } from "react"

const githubAccessToken = process.env.REACT_APP_GITHUB_ACCESS_TOKEN;

export default function useCard (userUrl) {
    const [loading, setLoading] = useState(false)
    const [user, setUser] = useState({})

    useEffect(() => {

        if (userUrl && !loading) {
            const headers = new Headers()
            headers.append('Accept', 'application/vnd.github.v3+json')
            headers.append('Authorization', `token ${githubAccessToken}`)

            setLoading(true)

            fetch (userUrl, { headers })
                .then((response) => response.json())
                .then((data) => setUser(data))
                .finally(() => {
                    setLoading(false)
                })
        }

    }, [userUrl])

    return {
        loading,
        user
    }
}