import Skeleton from "react-loading-skeleton"

export default function SkeletonText ({ loading, children, ...props }) {
    return loading ? <Skeleton {...props} /> : children
}