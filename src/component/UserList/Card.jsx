import Skeleton from "react-loading-skeleton";
import SkeletonText from "../SkeletonText";
import useCard from "../../hooks/useCard";
import Stars from "../Stars";

export default function Card({ url, avatar_url: avatarUrl, }) {
    const { loading, user } = useCard(url)

    return (
        <a href={user.html_url} target="_blank" className="border border-solid border-gray-200 rounded px-3 py-1 my-2 flex gap-2 h-28">
            {
                loading 
                    ? <Skeleton style={{ width: '5em', height: '5em' }} circle />
                    : <img className="w-20 h-20 my-auto rounded-full" src={avatarUrl} alt={user.name} />
            }

            <div className="flex flex-col justify-start my-2 ml-4 basis-1/4">
                <span className="w-full text-lg font-bold">
                    <SkeletonText loading={loading} height="1em" width="200px">{user.name ?? user.login}</SkeletonText>
                </span>

                <span className="text-xs">
                    <SkeletonText loading={loading} height=".5em" width="50px">{user.email}</SkeletonText>
                </span>

                <span className="text-xs">
                    <SkeletonText loading={loading} height=".5em" width="50px">
                        {user.followers} followers
                    </SkeletonText>
                </span>

            </div>

            <div className="my-2 basis-3/4">
                <span className="h-full">
                    <SkeletonText loading={loading} count={3} height="10px">
                        <p className="text-sm">
                            {user.bio}
                        </p>
                    </SkeletonText>
                </span>
            </div>

        </a>
    )
}