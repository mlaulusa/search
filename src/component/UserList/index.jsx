import { useEffect, useState, useCallback } from 'react'
import Pagination from './Pagination'
import Card from './Card';

const githubAccessToken = process.env.REACT_APP_GITHUB_ACCESS_TOKEN;

export default function UserList ({ searchTerm }) {
    const [users, setUsers] = useState([]) 
    const [total, setTotal] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [perPage, setPerPage] = useState(5)

    const onPageChange = useCallback((page) => {
        setCurrentPage(page)
    }, [setCurrentPage])

    useEffect(() => {

        if (searchTerm) {
            const searchParams = new URLSearchParams()
            searchParams.append('q', searchTerm)
            searchParams.append('page', currentPage)
            searchParams.append('per_page', perPage)

            const headers = new Headers()
            headers.append('Accept', 'application/vnd.github.v3+json')
            headers.append('Authorization', `token ${githubAccessToken}`)

            fetch(
                `https://api.github.com/search/users?${searchParams.toString()}`,
                {
                    method: 'GET',
                    headers,
                }
            )
                .then((response) => response.json())
                .then(({ incomplete_results, total_count, items }) => {
                    setTotal(total_count)
                    setUsers(items)
                })
        }

    }, [searchTerm, setTotal, setUsers, currentPage, perPage])

    return (

        <div className="h-full">

            <div className="w-1/2 mx-auto px-2 pt-2 text-lg">{total} results</div>

            <div className="w-1/2 mx-auto px-2 py-4">
                <ul>
                    {
                        users.map((user) => (
                            <Card 
                                key={user.id} 
                                {...user}
                            />
                        ))
                    }
                </ul>
            </div>

            <div className="w-full">
                <Pagination onPageChange={onPageChange} total={total} perPage={perPage} currentPage={currentPage} />
            </div>

        </div>

    )
}