import { useMemo, useCallback } from 'react'
import { number, func } from 'prop-types'
import classnames from 'classnames'

export default function Pagination ({ total, perPage, onPageChange, currentPage }) {
    const totalPages = useMemo(() => Math.ceil(total / perPage), [total, perPage])

    const onClick = useCallback((event) => {
        onPageChange(Number(event.target.textContent))
    }, [onPageChange])

    return (
        <ul className="w-1/5 text-center mx-auto flex flex-row justify-center gap-1">
            {
                Array(totalPages)
                    .fill(1)
                    .map((_, index) => index + 1)
                    .filter((page) => (page > (currentPage - 5) && page <= currentPage) || (page < (currentPage + 5) && page >= currentPage))
                    .map((page) => <li 
                                        key={`page ${page}`}
                                        className={classnames({ 'font-bold': currentPage === page }, 'cursor-pointer')} 
                                        onClick={onClick}
                                    >
                                        {page}
                                    </li>)
            }
        </ul>
    )
}

Pagination.propTypes = {
    total: number,
    perPage: number,
    onPageChange: func.isRequired,
    currentPage: number.isRequired,
}

Pagination.defaultProps = {
    total: 0,
    perPage: 30
}
