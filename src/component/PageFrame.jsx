import { Outlet  } from "react-router-dom"

export default function PageFrame () {
    return (
        <div className="h-screen">
            <div className="w-full h-8 bg-blueGreen"></div>

            <Outlet />

        </div>
    )
}