import { useEffect } from 'react'

const githubAccessToken = process.env.REACT_APP_GITHUB_ACCESS_TOKEN;

export default function Stars ({ starredUrl }) {
    useEffect(() => {

        const headers = new Headers()
        headers.append('Accept', 'application/vnd.github.v3+json')
        headers.append('Authorization', `token ${githubAccessToken}`)

        fetch (starredUrl.replace(/\{.+\}/g, ''), { headers })
            .then((response) => response.json())
            .then(console.log)

    }, [starredUrl])
    return (
        <p>
            {starredUrl}
        </p>
    )
}