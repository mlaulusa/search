import { useCallback, useState } from 'react'

export default function Search ({ onSubmit }) {
    const [searchTerm, setSearchTerm] = useState('')

    const onChange = useCallback((event) => {
        setSearchTerm(event.target.value)
    }, [setSearchTerm])

    const onClick = useCallback(() => {
        onSubmit(searchTerm)
    }, [onSubmit, searchTerm])

    return (
        <div className="w-1/3 px-8 flex flex-col items-start mx-auto">
            <label className="form-label w-full mt-2" htmlFor="input">User Search</label>
            <input 
                id="input" 
                className="form-control px-1 mb-2 border border-solid rounded"
                type="text" 
                onChange={onChange} 
                value={searchTerm} 
                />

            <button className="py-1 px-2 rounded text-right bg-blue-400" onClick={onClick}>Search</button>
        </div>
    )
}