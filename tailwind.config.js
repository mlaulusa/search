/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        coral: '#FF8370',
        blueGreen: '#00B1B0',
        freesia: '#FEC84D',
        fuchsia: '#E42256',
        tan: '#EFE7BC',
      }
    },
  },
  plugins: [],
}
