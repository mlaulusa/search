FROM nginx:alpine

WORKDIR /app

COPY ./build ./static
COPY ./nginx.conf /etc/nginx/nginx.conf